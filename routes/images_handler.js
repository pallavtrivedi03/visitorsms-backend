var express = require('express');
var router = express.Router();
const fs = require('fs');
const path = require('path');

router.get('/',function(req,res) {
    
});

router.get('/:image', function(req,res) {
    var imageName = req.params.image;
    console.log("Image name is "+imageName);
    
    if (imageName.indexOf("qr_") >= 0) {
        var img = fs.readFileSync(path.join(__dirname, '../qr_codes/'+imageName.substring(3)));
        res.writeHead(200, {'Content-Type': 'image/gif' });
        res.end(img, 'binary');
    } else {
        console.log("Checkk0---------------- "+imageName );
        
        var img = fs.readFileSync(path.join(__dirname, '../visitor_images/'+imageName));
        console.log("path is "+path.join(__dirname, '../visitor_images/'+imageName));
        
        res.writeHead(200, {'Content-Type': 'image/gif' });
        res.end(img, 'binary');
    }
});

module.exports = router;