var express = require('express');
var router = express.Router();
var employeesController = require('../controllers/employees_controller');

router.get('/',function(req,res) {
    employeesController.getEmployees(req,res);
});

router.get('/details/:code', function(req,res) {
    employeesController.getEmployeeDetails(req,res);
});

module.exports = router;