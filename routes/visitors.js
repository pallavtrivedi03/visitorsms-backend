const express = require('express');
const router = express.Router();
const visitorController = require('../controllers/visitors_controller');

router.post('/register',function(req,res,next) {
    visitorController.registerGuest(req,res);    
});

router.post('/checkin',function(req,res,next) {
    visitorController.checkinGuest(req,res);    
});

router.post('/checkout',function(req,res,next) {
    visitorController.checkoutGuest(req,res);    
});


module.exports = router;