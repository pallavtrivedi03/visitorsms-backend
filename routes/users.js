var express = require('express');
var router = express.Router();
var usersController = require('../controllers/users_controller');


router.post('/activate',function(req,res) {
    usersController.activateUser(req,res);
});

router.post('/login', function(req,res) {
    usersController.login(req,res);
});

router.post('/token', function(req,res) {
    console.log("Request for token received");
    
    usersController.getToken(req,res);
});

module.exports = router;