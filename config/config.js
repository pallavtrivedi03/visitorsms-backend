'use strict';
var config = require('./config.json');

config.mysql = config.mysqlConfigs["mysql"];

Object.keys(config).forEach((key) => {
    process.env[key] = JSON.stringify(config[key]);
});