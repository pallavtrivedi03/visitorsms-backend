'use strict';
const mysql = require('mysql');

console.log(process.env.mysql);

const nysqlConfig = JSON.parse(process.env.mysql);
var db = mysql.createConnection(nysqlConfig);

module.exports.executeQuery = function(queryStr,resultHandler){
    console.log("Query is "+queryStr);
    db.query(queryStr, resultHandler);
};