// Download the helper library from https://www.twilio.com/docs/node/install
// Your Account Sid and Auth Token from twilio.com/console
// DANGER! This is insecure. See http://twil.io/secure

const accountSid = 'AC51ba36fce0f28d0a67a99d72a3a47d1b';
const authToken = 'dd14e9595a52e5b2c8e3bb4125dd2c43';
const client = require('twilio')(accountSid, authToken);
const fromNumber = '+16315134228';

module.exports.sendSMS = function(to,body) {
    console.log("About to send sms to "+to);
    
    client.messages
  .create({
     body: body,
     from: fromNumber,
     to: to
   })
  .then(message => console.log(message.sid));

}
