'use strict'
const databaseConnector = require('./database_connector.js');

module.exports.getEmployees = function(callback) {
    databaseConnector.executeQuery("select * from employees", (error,result) => {
        if(!error) {
            callback(JSON.stringify(result, undefined, 2),null);
        } else {
            callback(null,error);
        }
    });
};

module.exports.getEmployeeDetails = function(params,email,callback) {
    if(params) {
        databaseConnector.executeQuery(`select * from employees where code = ${params.code}`, (error,result) => {
            if(!error) {
                callback(JSON.stringify(result, undefined, 2),null);
            } else {
                callback(null,error);
            }
        });
    } else {
        databaseConnector.executeQuery(`select * from employees where email = "${email}"`, (error,result) => {
            if(!error) {
                callback(JSON.stringify(result, undefined, 2),null);
            } else {
                callback(null,error);
            }
        });
    }
  
};

module.exports.activateUser = function(code,email,password,callback) {
    databaseConnector.executeQuery(`insert into users (code,email,password) values (${code},"${email}","${password}")`, (error,result) => {
        if(!error) {
            callback(true);
        } else {
            console.log(error);
            
            callback(false);
        }
    });
}

module.exports.registerGuest = function(params,callback) {
    databaseConnector.executeQuery(`insert into visitors 
    (first_name,last_name,company,purpose,visitee,email,phone,image,date,invitee,is_group_representative,group_size,checkin,checkout) 
    values ('${params.first_name}','${params.last_name}','${params.company}','${params.purpose}',${params.visitee},
        '${params.email}','${params.phone}','${params.image}','${params.date}',${params.invitee},
        ${params.is_group_representative},${params.group_size},'${params.checkin}',
        '${params.checkout}')`, async (error,result) => {
        if(!error) {
            await getVisitorDetails(params.email,params.date,function(details,err){
                if(details) {
                    callback(JSON.parse(details));
                } else {
                    callback(false);        
                }
            })
        } else {
            callback(false);
        }
    });
}

function getVisitorDetails(email,date,visitorDetails) {
    databaseConnector.executeQuery(`select * from visitors where email = '${email}' AND date =${date}`, (error,result) => {
        if(!error) {
            visitorDetails(JSON.stringify(result, undefined, 2),null);
        } else {
            visitorDetails(null,error);
        }
    });
}

module.exports.getVisitorDetails= function (vId,visitorDetails) {
    databaseConnector.executeQuery(`select * from visitors where v_id = '${vId}'`, (error,result) => {
        if(!error) {
            visitorDetails(JSON.stringify(result, undefined, 2),null);
        } else {
            visitorDetails(null,error);
        }
    });
}

module.exports.login = function(email,callback) {
    databaseConnector.executeQuery(`select password from users where email = "${email}"`, (error,result) => {
        if(!error) {
            console.log("Retreived successfully");
            let res = JSON.stringify(result,undefined,2);
            console.log("strigifies is "+res);
            
            callback(res,null);
        } else {
            console.log("Error is "+error);
            
            callback(null,error);
        }
    })
}

module.exports.checkin = function(vId,checkinTime,callback) {
    databaseConnector.executeQuery(`update visitors set checkin = ${checkinTime} where v_id = ${vId}`, (error, result) => {
        if (!error) {
            callback(true,null);
        } else {
            console.log("Error is: "+error);
            
            callback(null,error);
        }
    })
}

module.exports.checkout = function(vId,checkoutTime,callback) {
    databaseConnector.executeQuery(`update visitors set checkout = ${checkoutTime} where v_id = ${vId}`, (error, result) => {
        if (!error) {
            callback(true,null);
        } else {
            console.log("Error is: "+error);
            
            callback(null,error);
        }
    })
}