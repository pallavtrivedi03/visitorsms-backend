'use strict'
var databaseHelper = require('./database_helper');
var jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const saltRounds = 10;
var config = require('../config/defaults');

module.exports.activateUser = function(req,res) {
    var code = req.body.code;
    var email = req.body.email.toLowerCase();
    var password = req.body.password;
    var hash = bcrypt.hashSync(password, saltRounds);
    databaseHelper.activateUser(code,email,hash,function(isSuccessful) {
        if(isSuccessful) {
          var expirationTime = { 'expiresIn': '12h' };
          var token = jwt.sign({ id: email}, config.secret, expirationTime);

          res.json({status:200, message:"Successfully registered", token:token});
        } else {
          res.json({status:500, message:"Internal server error"}); 
        }
    });
};

module.exports.login = function(req,res) {
    var email = req.body.email.toLowerCase();
    var password = req.body.password;
    var hash = bcrypt.hashSync(password,saltRounds);
    var expirationTime = { 'expiresIn': '12h' };
    var token = jwt.sign({ id: email}, config.secret, expirationTime);

    databaseHelper.login(email,async function(result,err) {
        if(result) {
            if(result.length > 0) {
                let parsedResult = JSON.parse(result);
                console.log("Hash "+hash);
                console.log("Password "+parsedResult[0].password);
                if(bcrypt.compareSync(password,parsedResult[0].password)) {
                    console.log("Check 2");
                    
                    await getEmployeeDetails(email,function(details,error) {
                        if(details) {
                            res.json({status:200,message:"Logged in successfully",details,token});
                        } else {
                            res.json({status:200, message:"Logged in successfully", details:{},token});
                        }
                    })
                } else {
                    res.json({status:404, message:"Incorrect username/password"});
                }
             } else {
                 res.json({status:404, message:"User not found"});
             }
        } else {
            res.json({status:500, message:"Inetrnal server error"});
        }
        
    });
}

module.exports.getToken = function(req,res) {

    if(req.body.deviceId) {
        var expirationTime = { 'expiresIn': '120h' };
        var token = jwt.sign({ id: req.body.deviceId}, config.secret, expirationTime);
        res.json({status:200, token});
    } else {
        res.json({status:404, message:"Device Id is required for getting token"});
    }
    
}

function getEmployeeDetails(email,callback) {
    console.log("check 1");
    
    databaseHelper.getEmployeeDetails(null, email, function(result,err){
        if(result) {
            console.log("RESULT IS"+result);

            callback(JSON.parse(result),null);
        } else {
            console.log("ERROR IS"+err);

            callback(null,err);
        }
    });
}
