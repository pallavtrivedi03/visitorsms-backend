'use strict'
var databaseHelper = require('./database_helper');


module.exports.getEmployees = function(req,res) {
    if (req.isAuth) {
        console.log("Fetch employees");
        databaseHelper.getEmployees(function(results,err) {
            if(results) {
                res.json({status:200, employees:JSON.parse(results)});    
            } else {
                res.json({status:500, error:err});
            }
        });
    } else {
        res.json({status:400,message:"Token is required to fetch employees"});
    }
    
};

module.exports.getEmployeeDetails = function(req,res) {
    if(req.isAuth) {
        databaseHelper.getEmployeeDetails(req.params, null, function(result,err){
            if(result) {
                
             res.json({status:200, details:JSON.parse(result)});
            } else {
                
                res.json({status:500, error:err});
            }
        });
    } else {
        res.json({status:400,message:"Token is required to fetch employee details"});
    }  
}