'use strict'
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const path = require('path');
const config = require('../config/defaults');
const smtpTransportConfig = require('nodemailer-smtp-transport');
const QRCode = require('qrcode');
const fs = require('fs');
var to, subject, html, mailOptions;


// Creating smtp object for sending mail (GMAIL)
var smtpTransport = nodemailer.createTransport({
  service: "gmail",
  host: "smtp.gmail.com",
  auth: {
    user: config.mail.email,
    pass: config.mail.password
  }
});


//Using relay service (Hungama)
// var smtpTransport = nodemailer.createTransport(smtpTransportConfig({
//     host: '103.16.47.18',
//     port: 25
//   }));

module.exports.sendInvitationMail = async function (recipientsDetail) {

  var details = recipientsDetail[0];
  console.log("Details are " + details);
  await getQrCode(details.v_id, function (qrPath) {
    to = details.email;
    console.log(to);
    subject = "Hungama: Invitation for " + details.purpose + " with " + details.visiteeDetails[0].name;
    //   html = `
    //   <html>
    //     <body>
    //         <p>Invitation for ${details.purpose} with ${details.visiteeDetails[0].name}</p>
    //         <p>Date: ${details.date}</p>
    //         <img id="qrCode"  width="100px;" height="100px;" src="${imageData}"></img>
    //     </body>
    //  </html>`;
    const imagePath = "http://hvisitorms.herokuapp.com/images/"+(details.image.split("/"))[details.image.split("/").length - 1];
    html = getMailBody(imagePath,details.first_name+" "+details.last_name,details.company,details.purpose+" with "+details.visiteeDetails[0].name,details.email,qrPath);
    prepareMailOption();
    sendMail();
  })
}

function getQrCode(vId, callback) {
  var url = `http://localhost:3000/?v_id=${vId}`;
  QRCode.toDataURL(url, { errorCorrectionLevel: 'L' })
    .then(url => {

      console.log("qr data is "+url);
      
      let qrPath = path.join(__dirname, '../qr_codes') + "/" + vId + "_" + Date.now() + ".png";
      console.log("qr path is " + qrPath);

      var base64Data = url.replace(/^data:image\/png;base64,/, "");
      fs.writeFile(qrPath, new Buffer(base64Data, "base64"), (err) => {
        if (err) throw err;
        callback("http://hvisitorms.herokuapp.com/images/qr_"+(qrPath.split("/"))[(qrPath.split("/").length - 1)]);
      });
    })
    .catch(err => {
      console.error(err)
    })

}

function prepareMailOption(attachments = null) {
  console.log("preparing options");

  if (attachments == null) {
    // setup email data with unicode symbols
    mailOptions = {
      to: to,
      from: config.mail.email,
      subject: subject,
      html: html
    };
  } else {
    mailOptions = {
      to: to,
      from: config.mail.email,
      subject: subject,
      html: html,
      attachments: attachments
    };
  }

  console.log(mailOptions);
}

// sends mail
function sendMail() {
  // send mail with defined transport object
  console.log("sending");
  smtpTransport.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
      return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
    // Preview only available when sending through an Ethereal account
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  });
}

function getMailBody(imagPath, name, company, purpose, email, qrPath) {
  return `
  <html>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</head>

<body>
    <br/>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="container-fluid" style="border:1px solid #cecece;">
                   <div class="container">
                       <br/>
                        <div class="row">
                                <div class="col-md-3">
                                    <img src="${imagPath}" width="150px;" height="150px;" alt="">
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-8">
                                    
                                    <div class="row">
                                       <b>Name:&nbsp;</b> <p>${name}</p>
                                    </div>
                                    <div class="row">
                                        <b>Company:&nbsp;</b> <p>${company}</p>
                                    </div>
                                    <div class="row">
                                        <b>Purpose:&nbsp;</b> <p>${purpose}</p>
                                    </div>
                                    <div class="row">
                                            <b>Email:&nbsp;</b> <p>${email}</p>
                                        </div>
                                </div>
                            </div>
                            <div class="row">
                                    <div class="col-md-8">
                                            <br/><br/>
                                        <p><i>Please scan this QR at reception desk for checkin and checkout.</i></p>
                                    </div>
                                    <div class="col-md-1"></div>
                                <div class="col-md-3">
                                        <img src="${qrPath}" width="150px;" height="150px;" alt="">
                                </div>
                            </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>`
}