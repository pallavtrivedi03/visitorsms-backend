'use strict'
const databaseHelper = require('./database_helper');
const mailController = require('./mail_controller');
const path = require('path');
const fs = require('fs');
const smsController = require('./sms_controller');

module.exports.registerGuest = async function(req,res) {
    if (req.isAuth) {
        console.log("Register Guest");

        if(req.body.checkin === "") {
            req.body.checkin = req.body.date;       //Pre register -> Dummy checkin time (of appointment date)
        } else {
            req.body.checkin = req.body.checkin;    //Walk In -> Directly Checkin
            //Send SMS to visitee
            // smsController.sendSMS('+918433719458','This is a test SMS');
        }
        
        let imagePath = path.join(__dirname,'../visitor_images')+"/"+req.body.first_name+"_"+Date.now()+".jpg";
        console.log("image path is "+imagePath);
        
        saveImageToDisk(req.body.image,imagePath);
        req.body.image = imagePath;

        databaseHelper.registerGuest(req.body,function(details,err) {
            if(details) {
                console.log(details);
                getVisiteeDetails(req.body.visitee,function(res,error){
                    if(res) {
                        details[0].visiteeDetails = res;
                        mailController.sendInvitationMail(details);
                    }
                });
                res.json({status:200, message:"Guest registered successfully."});    
            } else {
                res.json({status:500, error:err});
            }
        });
    } else {
        res.json({status:400,message:"Token is required to register user"});
    }
};

module.exports.checkinGuest = function (req, res) {
    if(req.isAuth) {
        if(req.body.vId) {
            const checkinTime = Date.now();
            databaseHelper.checkin(req.body.vId,checkinTime,function(result,err){
                if(!err) {
                    databaseHelper.getVisitorDetails(req.body.vId,function(details,error){
                        if(!error) {
                            let visitorDetails = JSON.parse(details)[0];
                            res.json({status:200, message: "Checked in successfully.", details:visitorDetails });
                        } else {
                            res.json({status:200, message: "Checked in successfully."})
                        }
                    })
                } else {
                    res.json({status:500, message: "Error checking in."})
                }
            });
        } else {
            res.json({status:400,message:"Visitor Id is required for checkin"});
        }
    } else {
        res.json({status:400,message:"Token is required for checkin"});
    }
}

module.exports.checkoutGuest = function (req, res) {
    if(req.isAuth) {
        if(req.body.vId) {
            const checkoutTime = Date.now();
            databaseHelper.checkout(req.body.vId,checkoutTime,function(result,err){
                if(!err) {
                    res.json({status:200, message: "Checked out successfully."})
                } else {
                    res.json({status:500, message: "Error checking out."})
                }
            });
        } else {
            res.json({status:400,message:"Visitor Id is required for checking out"});
        }
    } else {
        res.json({status:400,message:"Token is required for checkout"});
    }
}

function saveImageToDisk(imageData,filePath) {
    fs.writeFile(filePath, new Buffer(imageData, "base64"), (err) => {
        if (err) throw err;
    
        console.log("The file was succesfully saved!");
    }); 
} 

function getVisiteeDetails(code,callback) {
    databaseHelper.getEmployeeDetails({code}, null, function(result,err){
        if(result) {
            console.log("parsed details are "+JSON.parse(result));
            
         callback(JSON.parse(result),null);
        } else {
            callback(null,err);
        }
    });
}